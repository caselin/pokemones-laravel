<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pokemon>
 */
class PokemonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tipos=['agua','tierra','fuego','volador','fantasma'];
        return [
            'nombre'=>$this->faker->firstName,
            'tipo'=>$this->faker->randomElement($tipos),
            'habilidades'=>$this->faker->text(140),
            'pokedex'=>$this->faker->numberBetween(1,1200),
        ];
    }
}
